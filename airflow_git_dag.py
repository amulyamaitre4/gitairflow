import json
import sys
import sys
import os
os.environ["GIT_PYTHON_REFRESH"] = "quiet"

import git
import airflow
from airflow.models import DAG, Variable
from airflow.operators.python_operator import PythonOperator

# Following are defaults which can be overridden later on
default_args = {
    'owner': 'Amulya',
    'depends_on_past': False,
    'start_date': airflow.utils.dates.days_ago(2)
}

airflow_git_dag = DAG('GitDag', default_args=default_args, catchup=False, schedule_interval=None)

# path = "./TEST2"
# clone = "https://amulyamaitre4@bitbucket.org/amulyamaitre4/sample.git"
# repo_name = "sample"

clone_repo = Variable.get('clone_repo')
repo_name = Variable.get('repo_name')
path_of_clone = Variable.get('path_of_clone')


def start():
    print("Start Process")


def clone_pull():
    if os.path.exists(os.path.join(path_of_clone, repo_name)):
        print("Pulling repo...")
        git.cmd.Git(os.path.join(path_of_clone, repo_name)).pull()
        print("Done Pulling repo...!")
    else:
        print("Cloning Repo...")
        git.Git(path_of_clone).clone(clone_repo)
        print("Done Cloning repo...!")


# Task to call etl process
startprocess = PythonOperator(
    task_id='start',
    # provide_context=True,
    python_callable=start,
    dag=airflow_git_dag
)

# Task to clone/pull repo
clonepull = PythonOperator(
    task_id='clone_pull',
    # provide_context=True,
    python_callable=clone_pull,
    dag=airflow_git_dag
)

startprocess.set_downstream(clonepull)
